package com.example.activityworker.config;

import com.example.activityworker.activity.WeatherActivitiesImpl;
import com.example.activityworker.repository.TemperatureDataRepository;
import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.example.workflowapi.workflow.WeatherWorkflow.TASK_LIST;

@Component
public class ActivityWorkerStarter {

    final
    WorkflowClient workflowClient;

    final
    TemperatureDataRepository temperatureDataRepository;

    public ActivityWorkerStarter(TemperatureDataRepository temperatureDataRepository, WorkflowClient workflowClient) {
        this.temperatureDataRepository = temperatureDataRepository;
        this.workflowClient = workflowClient;
    }

    @PostConstruct
    public void startWorkerFactory() {
        WorkerFactory factory = WorkerFactory.newInstance(workflowClient);
        Worker worker = factory.newWorker(TASK_LIST);
        worker.registerActivitiesImplementations(new WeatherActivitiesImpl(temperatureDataRepository));
        factory.start();
    }





}

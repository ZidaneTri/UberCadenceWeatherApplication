package com.example.activityworker.repository;

import com.example.activityworker.entity.TemperatureData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemperatureDataRepository extends JpaRepository<TemperatureData, Long> {
}
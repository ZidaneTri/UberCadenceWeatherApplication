package com.example.workflowworker.workflow;

import com.example.workflowapi.workflow.WeatherWorkflow;
import com.example.workflowapi.activity.WeatherActivities;
import com.uber.cadence.workflow.Workflow;

public class WeatherWorkflowImpl implements WeatherWorkflow {

    private final WeatherActivities activities =
            Workflow.newActivityStub(WeatherActivities.class);


    @Override
    public void getWeather(String name) {

        String temperature = activities.retrieveWeatherData(name);
        activities.storeWeatherData(temperature, name);
    }
}

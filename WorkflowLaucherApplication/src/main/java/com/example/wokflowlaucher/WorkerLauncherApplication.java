package com.example.wokflowlaucher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkerLauncherApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkerLauncherApplication.class, args);
    }

}

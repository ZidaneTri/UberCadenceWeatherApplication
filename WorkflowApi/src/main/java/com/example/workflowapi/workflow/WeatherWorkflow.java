package com.example.workflowapi.workflow;

import com.uber.cadence.workflow.WorkflowMethod;

public interface WeatherWorkflow {

     String TASK_LIST = "WeatherTaskList";

     String WEATHER_DOMAIN = "test-domain";

    @WorkflowMethod(executionStartToCloseTimeoutSeconds = 120, taskList = TASK_LIST)
    void getWeather(String name);
}
